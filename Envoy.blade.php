@servers(['web' => 'deployer@pathfinder.42nd.co'])

@setup
    $repository = 'git@gitlab.com:MartinPeverelli/pathfinder.git';
    $releases_dir = '/var/www/pathfinder/releases';
    $app_dir = '/var/www/pathfinder';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;

    function logMessage($message) {
    return "echo '\033[32m" .$message. "\033[0m';\n";
    }
@endsetup

@story('deploy')
    clone_repository
    run_composer
    generate_assets
    update_symlinks
    migrate_database
    bless_release
    clean_old_releases
    finish_deploy
@endstory

@task('clone_repository')
    {{ logMessage("🌀  Cloning repository...") }}
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
@endtask

@task('run_composer')
    {{ logMessage("🚚  Running Composer...") }}
    cd {{ $new_release_dir }}
    composer install --prefer-dist --no-scripts -q -o
@endtask

@task('generate_assets')
    {{ logMessage("🌅  Generating assets…") }}
    cd {{ $new_release_dir }};
    npm install
    npm run production --progress false
@endtask

@task('update_symlinks')
    {{ logMessage("🔗  Updating symlinks to persistent data...") }}
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env
@endtask

@task('migrate_database')
    {{ logMessage("🙈  Migrating database...") }}
    cd {{ $new_release_dir }}
    php artisan migrate --force;
@endtask

@task('bless_release')
    {{ logMessage("🙏  Blessing new release...") }}
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
    cd {{ $new_release_dir }}
    php artisan config:clear
    {{--php artisan view:clear--}}
    php artisan cache:clear
    php artisan route:clear
    php artisan config:cache
    {{--php artisan route:cache--}}
    php artisan queue:restart

    sudo service php7.1-fpm restart
    sudo service nginx restart
@endtask

@task('clean_old_releases')
    {{ logMessage("🚾  Cleaning up old releases...") }}
    cd {{ $releases_dir }}
    ls -dt {{ $releases_dir }}/* | tail -n +6 | xargs -d "\n" rm -rf;
@endtask

@task('finish_deploy')
    {{ logMessage("🚀  Application deployed!") }}
@endtask

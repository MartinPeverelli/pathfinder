<?php

namespace App\Console\Commands;

use App\Spell;
use Illuminate\Console\Command;

class SyncSpells extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:spells';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync spells from https://docs.google.com/spreadsheets/d/1cuwb3QSvWDD7GG5McdvyyRBpqycYuKMRsXgyrvxvLFI/edit#gid=1144712474';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Sheets::setService(\Google::make('sheets'));
        $spreadsheet = \Sheets::spreadsheet('1cuwb3QSvWDD7GG5McdvyyRBpqycYuKMRsXgyrvxvLFI');
        $sheets = $spreadsheet->sheetList();
        $rows = $spreadsheet->sheet(array_shift($sheets))
//            ->range('A1:CO2')
            ->get();

        $header = $rows->pull(0);
        foreach ($rows as $row) {
            $item = array_combine($header, $row);
            Spell::fromSpreadsheet($item);
        }
    }
}

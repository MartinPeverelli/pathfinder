<?php

namespace App\Filters;

use Illuminate\Http\Request;

abstract class Filters
{
    protected $request;

    protected $builder;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    abstract public function apply($builder);

    public function equals($attribute, $value)
    {
        return $this->builder->where($attribute, '=', $value);
    }

    public function boolean($attribute, $value)
    {
        return $this->equals($attribute, $value);
    }

    public function like($attribute, $value)
    {
        $string = strtolower($value);
        return $this->builder->whereRaw("LOWER({$attribute}) LIKE '%{$string}%'");
    }

    public function in($attribute, $value)
    {
        return $this->builder->whereIn($attribute, $value);
    }
}

<?php

namespace App\Filters;

class SpellFilters extends Filters
{
    public function apply($builder)
    {
        $this->builder = $builder;

        foreach ($this->request->all() as $key => $value) {
            if ($value == '') {
                continue;
            }
            switch ($key) {
                case 'name':
                    $this->like($key, $value);
                    break;
                case 'verbal':
                case 'somatic':
                case 'material':
                case 'focus':
                case 'divine_focus':
                case 'area':
                case 'dismissible':
                case 'shapeable':
                case 'saving_throw':
                case 'spell_resistence':
                    $this->equals($key, $value);
                    break;
            }
        }

        if ($this->request->get('class') && is_numeric($this->request->get('level'))) {
            $this->equals($this->request->get('class'), $this->request->get('level'));
        }

        return $this->builder;
    }
}

<?php

namespace App\Http\Controllers;

use App\Filters\SpellFilters;
use App\Spell;
use Illuminate\Http\Request;

class SpellsController extends Controller
{
    public function index()
    {
        $filters = new SpellFilters(request());
        $spells = Spell::filter($filters)->orderBy('name')->limit(100)->get();
        return view('spells.index', compact('spells'));
    }

    public function show($id)
    {
        $spell = Spell::find($id);
        return view('spells.show', compact('spell'));
    }
}

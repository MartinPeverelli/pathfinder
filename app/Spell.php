<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Spell extends Model
{
    protected $guarded = [];

    public static function fromSpreadsheet($data)
    {
        $data['saving_throw_text'] = $data['saving_throw'];
        $data['saving_throw'] = !starts_with(strtolower($data['saving_throw']), 'no');

        $data['spell_resistence_text'] = $data['spell_resistence'];
        $data['spell_resistence'] = !starts_with(strtolower($data['spell_resistence']), 'no');

        $data['augmented_text'] = $data['augmented'];
        $data['augmented'] = !empty($data['augmented']);

        $data['area_text'] = $data['area'];
        $data['area'] = !empty($data['area']);

        foreach ($data as $k => $v) {
            $data[$k] = ($v === 'NULL') ? null : $v;
        }

        return self::updateOrCreate($data);
    }

    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }
}

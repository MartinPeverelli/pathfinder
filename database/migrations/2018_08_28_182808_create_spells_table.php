<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpellsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spells', function (Blueprint $table) {
            $table->string('name');
            $table->string('school')->nullable();
            $table->string('subschool')->nullable();
            $table->string('descriptor')->nullable();
            $table->string('spell_level')->nullable();
            $table->string('casting_time')->nullable();
            $table->string('components')->nullable();
            $table->boolean('costly_components')->default(false);
            $table->string('range')->nullable();
            $table->boolean('area')->default(false);
            $table->string('effect')->nullable();
            $table->string('targets')->nullable();
            $table->string('duration')->nullable();
            $table->boolean('dismissible')->default(false);
            $table->boolean('shapeable')->default(false);
            $table->boolean('saving_throw')->default(false);
            $table->boolean('spell_resistence')->default(false);
            $table->text('description')->nullable();
            $table->text('description_formated')->nullable();
            $table->string('source')->nullable();
            $table->text('full_text')->nullable();
            $table->boolean('verbal')->default(false);
            $table->boolean('somatic')->default(false);
            $table->boolean('material')->default(false);
            $table->boolean('focus')->default(false);
            $table->boolean('divine_focus')->default(false);
            $table->integer('sor')->nullable();
            $table->integer('wiz')->nullable();
            $table->integer('cleric')->nullable();
            $table->integer('druid')->nullable();
            $table->integer('ranger')->nullable();
            $table->integer('bard')->nullable();
            $table->integer('paladin')->nullable();
            $table->integer('alchemist')->nullable();
            $table->integer('summoner')->nullable();
            $table->integer('witch')->nullable();
            $table->integer('inquisitor')->nullable();
            $table->integer('oracle')->nullable();
            $table->integer('antipaladin')->nullable();
            $table->integer('magus')->nullable();
            $table->integer('adept')->nullable();
            $table->string('deity')->nullable();
            $table->integer('sla_level')->nullable();
            $table->string('domain')->nullable();
            $table->text('short_description')->nullable();
            $table->boolean('acid')->default(false);
            $table->boolean('air')->default(false);
            $table->boolean('chaotic')->default(false);
            $table->boolean('cold')->default(false);
            $table->boolean('curse')->default(false);
            $table->boolean('darkness')->default(false);
            $table->boolean('death')->default(false);
            $table->boolean('disease')->default(false);
            $table->boolean('earth')->default(false);
            $table->boolean('electricity')->default(false);
            $table->boolean('emotion')->default(false);
            $table->boolean('evil')->default(false);
            $table->boolean('fear')->default(false);
            $table->boolean('fire')->default(false);
            $table->boolean('force')->default(false);
            $table->boolean('good')->default(false);
            $table->boolean('language_dependent')->default(false);
            $table->boolean('lawful')->default(false);
            $table->boolean('light')->default(false);
            $table->boolean('mind_affecting')->default(false);
            $table->boolean('pain')->default(false);
            $table->boolean('poison')->default(false);
            $table->boolean('shadow')->default(false);
            $table->boolean('sonic')->default(false);
            $table->boolean('water')->default(false);
            $table->string('linktext')->nullable();
            $table->increments('id');
            $table->integer('material_costs')->nullable();
            $table->string('bloodline')->nullable();
            $table->string('patron')->nullable();
            $table->text('mythic_text')->nullable();
            $table->boolean('augmented')->default(false);
            $table->boolean('mythic')->default(false);
            $table->integer('bloodrager')->nullable();
            $table->integer('shaman')->nullable();
            $table->integer('psychic')->nullable();
            $table->integer('medium')->nullable();
            $table->integer('mesmerist')->nullable();
            $table->integer('occultist')->nullable();
            $table->integer('spiritualist')->nullable();
            $table->integer('skald')->nullable();
            $table->integer('investigator')->nullable();
            $table->integer('hunter')->nullable();
            $table->text('haunt_statistics')->nullable();
            $table->boolean('ruse')->default(false);
            $table->boolean('draconic')->default(false);
            $table->boolean('meditative')->default(false);
            $table->integer('summoner_unchained')->nullable();

            // custom columns
            $table->string('area_text')->nullable();
            $table->text('saving_throw_text')->nullable();
            $table->text('spell_resistence_text')->nullable();
            $table->text('augmented_text')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spells');
    }
}

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Pathfinder') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="border-t-4">
<div id="app">
    <nav id="menu" class="bg-grey-lightest">
        @stack('slideout')
    </nav>

    <div id="panel" class="bg-white min-h-screen font-sans font-normal antialiased shadow-lg">
        <div  class="flex flex-col w-full fixed pin-l pin-y">

            @include('layouts.navbar')

            @stack('toolbar')

            <div class="px-2 flex-1 overflow-y-scroll">
                <div class="container mx-auto pt-2">

                    @yield('content')

                </div>
            </div>

        </div>
    </div>
</div>
</body>
</html>

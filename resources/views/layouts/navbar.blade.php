<nav class="flex items-center justify-between flex-wrap p-2 border-b shadow">
    <div class="flex items-center flex-no-shrink mr-6">
        <a href="{{ url('/') }}">
            <span class="font-semibold text-xl">
                {{ config('app.name', 'Pathfinder') }}
            </span>
        </a>
    </div>
    <div class="block flex items-center w-auto">
        <div>
            <a href="{{ route('spells.index') }}" class="inline-block mt-0">
                Spells
            </a>
        </div>
    </div>
</nav>
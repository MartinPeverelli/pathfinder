
<div>
    <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1" for="grid-state">
        {{ $label }}
    </label>
    <div class="relative">
        <select class="block appearance-none w-full bg-white border border-grey-light text-grey-darker py-2 px-4 pr-8 rounded leading-tight focus:outline-none"
            name="{{ $name }}"
        >
            <option value=""></option>
            @foreach($options as $k => $v)
                <option value="{{ $k }}" {{ request($name) == $k ? 'selected' : false }}>{{ $v }}</option>
            @endforeach
        </select>
        <div class="pointer-events-none absolute pin-y pin-r flex items-center px-2 text-grey-darker">
            <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
        </div>
    </div>
</div>
<div class="p-2 flex-1 overflow-hidden">

    <form class="w-full" action="{{ route('spells.index') }}" method="get">
        <div class="flex flex-wrap mb-2">
            <div class="w-full">
                <div class="flex flex-wrap items-stretch w-full relative">
                    <input class="flex-shrink flex-grow flex-auto leading-normal w-px flex-1 bg-white text-grey-darker border border-r-0 border-grey rounded-l py-2 px-4 leading-tight focus:outline-none"
                           type="text"
                           placeholder="Spell Name"
                           name="name"
                           value="{{ request('name') }}"
                    >
                    <button class="flex items-center leading-normal bg-grey-lighter rounded rounded-l-none border border-grey px-3 whitespace-no-wrap text-grey-dark text-sm focus:outline-none">
                        Search
                    </button>
                </div>
            </div>
        </div>
        <div class="flex flex-wrap mb-2">
            <div class="w-2/3">
                @include('partials.select', ['label' => 'Class', 'name' => 'class', 'options' => [
                    'adept' => 'Adept',
                    'alchemist' => 'Alchemist',
                    'antipaladin' => 'Anti-Paladin',
                    'bard' => 'Bard',
                    'bloodrager' => 'Bloodrager',
                    'cleric' => 'Cleric',
                    'druid' => 'Druid',
                    'hunter' => 'Hunter',
                    'inquisitor' => 'Inquisitor',
                    'investigator' => 'Investigator',
                    'magus' => 'Magus',
                    'medium' => 'Medium',
                    'mesmerist' => 'Mesmerist',
                    'occultist' => 'Occultist',
                    'oracle' => 'Oracle',
                    'paladin' => 'Paladin',
                    'psychic' => 'Psychic',
                    'ranger' => 'Ranger',
                    'shaman' => 'Shaman',
                    'skald' => 'Skald',
                    'sor' => 'Sorcerer',
                    'spiritualist' => 'Spiritualist',
                    'summoner' => 'Summoner',
                    'summoner_unchained' => 'Summoner Unchained',
                    'witch' => 'Witch',
                    'wiz' => 'Wizard',
                ]])
            </div>
            <div class="w-1/3 pl-1">
                @include('partials.select', ['label' => 'Level', 'name' => 'level', 'options' => range(0,9)])
            </div>
        </div>
        <div class="flex flex-wrap mb-2">
            <div class="w-1/2">
                <?php
                $options = \App\Spell::select('school')->distinct()->pluck('school')->toArray();
                $options = array_combine($options, array_map(function($v) { return ucwords($v);}, $options));
                ?>
                @include('partials.select', ['label' => 'School', 'name' => 'school', 'options' => $options])
            </div>
            <div class="w-1/2 pl-1">
                <?php
                $options = \App\Spell::select('source')->distinct()->pluck('source')->toArray();
                $options = array_combine($options, array_map(function($v) { return ucwords($v);}, $options));
                ?>
                @include('partials.select', ['label' => 'Source', 'name' => 'source', 'options' => $options])
            </div>
        </div>
        <div class="flex flex-wrap mb-2">
            <div class="w-1/2">
                <tristate-button :label="'Verbal'" :name="'verbal'" :selected="'{{ request('verbal') }}'"></tristate-button>
            </div>
            <div class="w-1/2 pl-1">
                <tristate-button :label="'Somatic'" :name="'somatic'" :selected="'{{ request('somatic') }}'"></tristate-button>
            </div>
        </div>
        <div class="flex flex-wrap mb-2">
            <div class="w-1/2">
                <tristate-button :label="'Material'" :name="'material'" :selected="'{{ request('material') }}'"></tristate-button>
            </div>
            <div class="w-1/2 pl-1">
                <tristate-button :label="'Focus'" :name="'focus'" :selected="'{{ request('focus') }}'"></tristate-button>
            </div>
        </div>
        <div class="flex flex-wrap mb-2">
            <div class="w-1/2">
                <tristate-button :label="'Divine Focus'" :name="'divine_focus'" :selected="'{{ request('divine_focus') }}'"></tristate-button>
            </div>
            <div class="w-1/2 pl-1">
                <tristate-button :label="'area'" :name="'area'" :selected="'{{ request('area') }}'"></tristate-button>
            </div>
        </div>
        <div class="flex flex-wrap mb-2">
            <div class="w-1/2">
                <tristate-button :label="'Dismissible'" :name="'dismissible'" :selected="'{{ request('dismissible') }}'"></tristate-button>
            </div>
            <div class="w-1/2 pl-1">
                <tristate-button :label="'Shapeable'" :name="'shapeable'" :selected="'{{ request('shapeable') }}'"></tristate-button>
            </div>
        </div>
        <div class="flex flex-wrap mb-2">
            <div class="w-1/2">
                <tristate-button :label="'Saving Throw'" :name="'saving_throw'" :selected="'{{ request('saving_throw') }}'"></tristate-button>
            </div>
            <div class="w-1/2 pl-1">
                <tristate-button :label="'SR'" :name="'spell_resistence'" :selected="'{{ request('spell_resistence') }}'"></tristate-button>
            </div>
        </div>
    </form>
</div>
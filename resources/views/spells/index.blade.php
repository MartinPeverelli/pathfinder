@extends('layouts.master')

@push('slideout')
    @include('spells.filters')
@endpush

@push('toolbar')
    <nav class="flex items-center flex-wrap p-2 border-b-2">
        <div class="inline-block mt-0">
            <a href="#" class="toggle-button">
                Filters
            </a>
        </div>
        <div class="inline-block mt-0 ml-2">
        </div>
    </nav>
@endpush

@section('content')
    <p class="mb-2 text-xs text-grey-dark text-center">{{ $spells->count() }} spells found</p>
    @foreach($spells as $spell)
        <div class="w-full mb-2">
            <div class="border border-grey-light bg-white rounded p-2 flex flex-col justify-between leading-normal">
                <a href="{{ route('spells.show', $spell->id) }}" class="text-black font-bold text-lg">{{ $spell->name }}</a>
                <p class="text-grey-darker text-sm">{{ $spell->spell_level }} <span class="text-grey-dark">{{ $spell->school }} ({{ $spell->subschool }}) [{{ $spell->descriptor }}]</span></p>
                <p class="text-grey-darker italic text-base">{{ $spell->short_description }}</p>
            </div>
        </div>
    @endforeach
@endsection

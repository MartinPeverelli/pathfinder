@extends('layouts.master')

@push('toolbar')
    <nav class="flex items-center flex-wrap p-2 border-b-2">
        <div class="inline-block mt-0">
            <a href="javascript:history.back()">Back</a>
        </div>
        <div class="inline-block mt-0 ml-2">
        </div>
    </nav>
@endpush

@section('content')
    <div class="w-full mb-2">
        <div class="border border-grey-light bg-white rounded p-2 flex flex-col justify-between leading-normal">
            <div class="text-black font-bold text-lg">{{ $spell->name }}</div>
            <p class="text-grey-darker text-sm">{{ $spell->spell_level }} <span class="text-grey-dark">{{ $spell->school }} ({{ $spell->subschool }}) [{{ $spell->descriptor }}]</span></p>
            <p class="text-grey-darker italic text-base">{{ $spell->short_description }}</p>

            <hr>

            @if($spell->casting_time)
                <p class="text-grey-darker text-sm"><span class="font-bold text-grey-darkest">Casting Time: </span>{{ $spell->casting_time }}</p>
            @endif
            @if($spell->components)
                <p class="text-grey-darker text-sm"><span class="font-bold text-grey-darkest">Components: </span>{{ $spell->components }}</p>
            @endif
            @if($spell->range)
                <p class="text-grey-darker text-sm"><span class="font-bold text-grey-darkest">Range: </span>{{ $spell->range }}</p>
            @endif
            @if($spell->area)
                <p class="text-grey-darker text-sm"><span class="font-bold text-grey-darkest">Area: </span>{{ $spell->area }}</p>
            @endif
            @if($spell->effect)
                <p class="text-grey-darker text-sm"><span class="font-bold text-grey-darkest">Effect: </span>{{ $spell->effect }}</p>
            @endif
            @if($spell->target)
                <p class="text-grey-darker text-sm"><span class="font-bold text-grey-darkest">Target: </span>{{ $spell->target }}</p>
            @endif
            @if($spell->duration)
                <p class="text-grey-darker text-sm"><span class="font-bold text-grey-darkest">Duration: </span>{{ $spell->duration }}</p>
            @endif
            @if($spell->saving_throw_text)
                <p class="text-grey-darker text-sm"><span class="font-bold text-grey-darkest">Saving Throw: </span>{{ $spell->saving_throw_text }}</p>
            @endif
            @if($spell->spell_resistence_text)
                <p class="text-grey-darker text-sm"><span class="font-bold text-grey-darkest">Spell Resistence: </span>{{ $spell->spell_resistence_text }}</p>
            @endif
            @if($spell->domain)
                <p class="text-grey-darker text-sm"><span class="font-bold text-grey-darkest">Domain: </span>{{ $spell->domain }}</p>
            @endif

            <hr>

            <p class="text-grey-darkest">{!! $spell->description_formated !!}</p>
        </div>
    </div>
@endsection
